# Configuration

CC		= gcc
LD		= gcc
AR		= ar
CFLAGS		= -g -std=gnu99 -Wall -Iinclude -fPIC
LDFLAGS		= -Llib
ARFLAGS		= rcs

# Variables

LIBRARY_HEADERS = include/lsort.h
LIBRARY_SOURCES = src/node.c src/list.c
LIBRARY_OBJECTS	= $(LIBRARY_SOURCES:.c=.o)
STATIC_LIBRARY  = lib/liblsort.a
LSORT_PROGRAM	= bin/lsort

TEST_SOURCES    = $(wildcard tests/test_*.c)
TEST_OBJECTS	= $(TEST_SOURCES:.c=.o)
TEST_PROGRAMS   = $(subst tests,bin,$(basename $(TEST_OBJECTS)))

# Rules

all:	$(LSORT_PROGRAM)

%.o:	%.c $(LIBRARY_HEADERS)
	@echo "Compiling $@"
	@$(CC) $(CFLAGS) -c -o $@ $<

bin/%:	tests/%.o $(STATIC_LIBRARY)
	@echo "Linking $@"
	@$(LD) $(LDFLAGS) -o $@ $^

$(LSORT_PROGRAM):	src/lsort.o $(STATIC_LIBRARY)
	@echo "Linking $@"
	@$(LD) $(LDFLAGS) -o $@ $^

$(STATIC_LIBRARY):	$(LIBRARY_OBJECTS)
	@echo "Linking $@"
	@$(AR) $(ARFLAGS) $@ $^


test:	$(TEST_PROGRAMS) $(LSORT_PROGRAM)
	@echo "Run Unit Test..."
	@for t in $(TEST_PROGRAMS); do 	\
	    echo "Testing $$t";		\
	    for i in $$(seq 0 $$($$t 2>&1 | tail -n 1 | awk '{print $$1}')); do \
		valgrind --leak-check=full $$t $$i > test.log 2>&1; 	\
		grep -q 'ERROR SUMMARY: 0' test.log || cat test.log;	\
		! grep -q 'Assertion' test.log || cat test.log;		\
	    done							\
	done
	@rm -f test.log
	@echo
	@echo "Run Functional Test..."
	@./bin/test_sort.sh

clean:
	@echo "Removing objects"
	@rm -f $(LIBRARY_OBJECTS) $(TEST_OBJECTS) src/*.o

	@echo "Removing static library"
	@rm -f $(STATIC_LIBRARY)

	@echo "Removing tests"
	@rm -f $(TEST_PROGRAMS)

	@echo "Removing lsort"
	@rm -f $(LSORT_PROGRAM)

.PRECIOUS: %.o
